/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.GeofoxApi);
  }
}(this, function(expect, GeofoxApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new GeofoxApi.TariffMetaDataResponse();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('TariffMetaDataResponse', function() {
    it('should create an instance of TariffMetaDataResponse', function() {
      // uncomment below and update the code to test TariffMetaDataResponse
      //var instane = new GeofoxApi.TariffMetaDataResponse();
      //expect(instance).to.be.a(GeofoxApi.TariffMetaDataResponse);
    });

    it('should have the property returnCode (base name: "returnCode")', function() {
      // uncomment below and update the code to test the property returnCode
      //var instane = new GeofoxApi.TariffMetaDataResponse();
      //expect(instance).to.be();
    });

    it('should have the property errorText (base name: "errorText")', function() {
      // uncomment below and update the code to test the property errorText
      //var instane = new GeofoxApi.TariffMetaDataResponse();
      //expect(instance).to.be();
    });

    it('should have the property errorDevInfo (base name: "errorDevInfo")', function() {
      // uncomment below and update the code to test the property errorDevInfo
      //var instane = new GeofoxApi.TariffMetaDataResponse();
      //expect(instance).to.be();
    });

    it('should have the property tariffKinds (base name: "tariffKinds")', function() {
      // uncomment below and update the code to test the property tariffKinds
      //var instane = new GeofoxApi.TariffMetaDataResponse();
      //expect(instance).to.be();
    });

    it('should have the property tariffLevels (base name: "tariffLevels")', function() {
      // uncomment below and update the code to test the property tariffLevels
      //var instane = new GeofoxApi.TariffMetaDataResponse();
      //expect(instance).to.be();
    });

    it('should have the property tariffCounties (base name: "tariffCounties")', function() {
      // uncomment below and update the code to test the property tariffCounties
      //var instane = new GeofoxApi.TariffMetaDataResponse();
      //expect(instance).to.be();
    });

    it('should have the property tariffZones (base name: "tariffZones")', function() {
      // uncomment below and update the code to test the property tariffZones
      //var instane = new GeofoxApi.TariffMetaDataResponse();
      //expect(instance).to.be();
    });

    it('should have the property tariffRings (base name: "tariffRings")', function() {
      // uncomment below and update the code to test the property tariffRings
      //var instane = new GeofoxApi.TariffMetaDataResponse();
      //expect(instance).to.be();
    });

  });

}));
