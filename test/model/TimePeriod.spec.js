/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.GeofoxApi);
  }
}(this, function(expect, GeofoxApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new GeofoxApi.TimePeriod();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('TimePeriod', function() {
    it('should create an instance of TimePeriod', function() {
      // uncomment below and update the code to test TimePeriod
      //var instane = new GeofoxApi.TimePeriod();
      //expect(instance).to.be.a(GeofoxApi.TimePeriod);
    });

    it('should have the property begin (base name: "begin")', function() {
      // uncomment below and update the code to test the property begin
      //var instane = new GeofoxApi.TimePeriod();
      //expect(instance).to.be();
    });

    it('should have the property end (base name: "end")', function() {
      // uncomment below and update the code to test the property end
      //var instane = new GeofoxApi.TimePeriod();
      //expect(instance).to.be();
    });

  });

}));
