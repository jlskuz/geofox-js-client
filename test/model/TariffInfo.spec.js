/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.GeofoxApi);
  }
}(this, function(expect, GeofoxApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new GeofoxApi.TariffInfo();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('TariffInfo', function() {
    it('should create an instance of TariffInfo', function() {
      // uncomment below and update the code to test TariffInfo
      //var instane = new GeofoxApi.TariffInfo();
      //expect(instance).to.be.a(GeofoxApi.TariffInfo);
    });

    it('should have the property tariffName (base name: "tariffName")', function() {
      // uncomment below and update the code to test the property tariffName
      //var instane = new GeofoxApi.TariffInfo();
      //expect(instance).to.be();
    });

    it('should have the property tariffRegions (base name: "tariffRegions")', function() {
      // uncomment below and update the code to test the property tariffRegions
      //var instane = new GeofoxApi.TariffInfo();
      //expect(instance).to.be();
    });

    it('should have the property extraFareType (base name: "extraFareType")', function() {
      // uncomment below and update the code to test the property extraFareType
      //var instane = new GeofoxApi.TariffInfo();
      //expect(instance).to.be();
    });

    it('should have the property ticketInfos (base name: "ticketInfos")', function() {
      // uncomment below and update the code to test the property ticketInfos
      //var instane = new GeofoxApi.TariffInfo();
      //expect(instance).to.be();
    });

    it('should have the property ticketRemarks (base name: "ticketRemarks")', function() {
      // uncomment below and update the code to test the property ticketRemarks
      //var instane = new GeofoxApi.TariffInfo();
      //expect(instance).to.be();
    });

  });

}));
