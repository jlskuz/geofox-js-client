/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import TariffRequest from '../model/TariffRequest';
import TariffResponse from '../model/TariffResponse';

/**
* TariffController service.
* @module api/TariffControllerApi
* @version 38.1.0
*/
export default class TariffControllerApi {

    /**
    * Constructs a new TariffControllerApi. 
    * @alias module:api/TariffControllerApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the getTariff operation.
     * @callback module:api/TariffControllerApi~getTariffCallback
     * @param {String} error Error message, if any.
     * @param {module:model/TariffResponse} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * @param {module:model/TariffRequest} tariffRequest 
     * @param {module:api/TariffControllerApi~getTariffCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/TariffResponse}
     */
    getTariff(tariffRequest, callback) {
      let postBody = tariffRequest;
      // verify the required parameter 'tariffRequest' is set
      if (tariffRequest === undefined || tariffRequest === null) {
        throw new Error("Missing the required parameter 'tariffRequest' when calling getTariff");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['APIKey1'];
      let contentTypes = ['application/json'];
      let accepts = ['*/*'];
      let returnType = TariffResponse;
      return this.apiClient.callApi(
        '/gti/public/getTariff', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
