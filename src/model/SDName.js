/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import Coordinate from './Coordinate';
import TariffDetails from './TariffDetails';

/**
 * The SDName model module.
 * @module model/SDName
 * @version 38.1.0
 */
class SDName {
    /**
     * Constructs a new <code>SDName</code>.
     * @alias module:model/SDName
     */
    constructor() { 
        
        SDName.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>SDName</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/SDName} obj Optional instance to populate.
     * @return {module:model/SDName} The populated <code>SDName</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new SDName();

            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('city')) {
                obj['city'] = ApiClient.convertToType(data['city'], 'String');
            }
            if (data.hasOwnProperty('combinedName')) {
                obj['combinedName'] = ApiClient.convertToType(data['combinedName'], 'String');
            }
            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'String');
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'String');
            }
            if (data.hasOwnProperty('coordinate')) {
                obj['coordinate'] = Coordinate.constructFromObject(data['coordinate']);
            }
            if (data.hasOwnProperty('tariffDetails')) {
                obj['tariffDetails'] = TariffDetails.constructFromObject(data['tariffDetails']);
            }
            if (data.hasOwnProperty('serviceTypes')) {
                obj['serviceTypes'] = ApiClient.convertToType(data['serviceTypes'], ['String']);
            }
            if (data.hasOwnProperty('hasStationInformation')) {
                obj['hasStationInformation'] = ApiClient.convertToType(data['hasStationInformation'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {String} name
 */
SDName.prototype['name'] = undefined;

/**
 * @member {String} city
 */
SDName.prototype['city'] = undefined;

/**
 * @member {String} combinedName
 */
SDName.prototype['combinedName'] = undefined;

/**
 * @member {String} id
 */
SDName.prototype['id'] = undefined;

/**
 * @member {module:model/SDName.TypeEnum} type
 * @default 'UNKNOWN'
 */
SDName.prototype['type'] = 'UNKNOWN';

/**
 * @member {module:model/Coordinate} coordinate
 */
SDName.prototype['coordinate'] = undefined;

/**
 * @member {module:model/TariffDetails} tariffDetails
 */
SDName.prototype['tariffDetails'] = undefined;

/**
 * @member {Array.<String>} serviceTypes
 */
SDName.prototype['serviceTypes'] = undefined;

/**
 * @member {Boolean} hasStationInformation
 */
SDName.prototype['hasStationInformation'] = undefined;





/**
 * Allowed values for the <code>type</code> property.
 * @enum {String}
 * @readonly
 */
SDName['TypeEnum'] = {

    /**
     * value: "UNKNOWN"
     * @const
     */
    "UNKNOWN": "UNKNOWN",

    /**
     * value: "STATION"
     * @const
     */
    "STATION": "STATION",

    /**
     * value: "ADDRESS"
     * @const
     */
    "ADDRESS": "ADDRESS",

    /**
     * value: "POI"
     * @const
     */
    "POI": "POI",

    /**
     * value: "COORDINATE"
     * @const
     */
    "COORDINATE": "COORDINATE"
};



export default SDName;

