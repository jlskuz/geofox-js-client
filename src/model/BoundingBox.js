/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import Coordinate from './Coordinate';

/**
 * The BoundingBox model module.
 * @module model/BoundingBox
 * @version 38.1.0
 */
class BoundingBox {
    /**
     * Constructs a new <code>BoundingBox</code>.
     * @alias module:model/BoundingBox
     * @param lowerLeft {module:model/Coordinate} 
     * @param upperRight {module:model/Coordinate} 
     */
    constructor(lowerLeft, upperRight) { 
        
        BoundingBox.initialize(this, lowerLeft, upperRight);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, lowerLeft, upperRight) { 
        obj['lowerLeft'] = lowerLeft;
        obj['upperRight'] = upperRight;
    }

    /**
     * Constructs a <code>BoundingBox</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/BoundingBox} obj Optional instance to populate.
     * @return {module:model/BoundingBox} The populated <code>BoundingBox</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new BoundingBox();

            if (data.hasOwnProperty('lowerLeft')) {
                obj['lowerLeft'] = Coordinate.constructFromObject(data['lowerLeft']);
            }
            if (data.hasOwnProperty('upperRight')) {
                obj['upperRight'] = Coordinate.constructFromObject(data['upperRight']);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/Coordinate} lowerLeft
 */
BoundingBox.prototype['lowerLeft'] = undefined;

/**
 * @member {module:model/Coordinate} upperRight
 */
BoundingBox.prototype['upperRight'] = undefined;






export default BoundingBox;

