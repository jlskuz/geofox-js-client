/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ServiceType model module.
 * @module model/ServiceType
 * @version 38.1.0
 */
class ServiceType {
    /**
     * Constructs a new <code>ServiceType</code>.
     * @alias module:model/ServiceType
     * @param simpleType {module:model/ServiceType.SimpleTypeEnum} 
     */
    constructor(simpleType) { 
        
        ServiceType.initialize(this, simpleType);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, simpleType) { 
        obj['simpleType'] = simpleType;
    }

    /**
     * Constructs a <code>ServiceType</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ServiceType} obj Optional instance to populate.
     * @return {module:model/ServiceType} The populated <code>ServiceType</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ServiceType();

            if (data.hasOwnProperty('simpleType')) {
                obj['simpleType'] = ApiClient.convertToType(data['simpleType'], 'String');
            }
            if (data.hasOwnProperty('shortInfo')) {
                obj['shortInfo'] = ApiClient.convertToType(data['shortInfo'], 'String');
            }
            if (data.hasOwnProperty('longInfo')) {
                obj['longInfo'] = ApiClient.convertToType(data['longInfo'], 'String');
            }
            if (data.hasOwnProperty('model')) {
                obj['model'] = ApiClient.convertToType(data['model'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/ServiceType.SimpleTypeEnum} simpleType
 */
ServiceType.prototype['simpleType'] = undefined;

/**
 * @member {String} shortInfo
 */
ServiceType.prototype['shortInfo'] = undefined;

/**
 * @member {String} longInfo
 */
ServiceType.prototype['longInfo'] = undefined;

/**
 * @member {String} model
 */
ServiceType.prototype['model'] = undefined;





/**
 * Allowed values for the <code>simpleType</code> property.
 * @enum {String}
 * @readonly
 */
ServiceType['SimpleTypeEnum'] = {

    /**
     * value: "BUS"
     * @const
     */
    "BUS": "BUS",

    /**
     * value: "TRAIN"
     * @const
     */
    "TRAIN": "TRAIN",

    /**
     * value: "SHIP"
     * @const
     */
    "SHIP": "SHIP",

    /**
     * value: "FOOTPATH"
     * @const
     */
    "FOOTPATH": "FOOTPATH",

    /**
     * value: "BICYCLE"
     * @const
     */
    "BICYCLE": "BICYCLE",

    /**
     * value: "AIRPLANE"
     * @const
     */
    "AIRPLANE": "AIRPLANE",

    /**
     * value: "CHANGE"
     * @const
     */
    "CHANGE": "CHANGE",

    /**
     * value: "CHANGE_SAME_PLATFORM"
     * @const
     */
    "CHANGE_SAME_PLATFORM": "CHANGE_SAME_PLATFORM"
};



export default ServiceType;

