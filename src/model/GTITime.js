/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The GTITime model module.
 * @module model/GTITime
 * @version 38.1.0
 */
class GTITime {
    /**
     * Constructs a new <code>GTITime</code>.
     * @alias module:model/GTITime
     * @param _date {String} 
     * @param time {String} 
     */
    constructor(_date, time) { 
        
        GTITime.initialize(this, _date, time);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, _date, time) { 
        obj['date'] = _date;
        obj['time'] = time;
    }

    /**
     * Constructs a <code>GTITime</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GTITime} obj Optional instance to populate.
     * @return {module:model/GTITime} The populated <code>GTITime</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new GTITime();

            if (data.hasOwnProperty('date')) {
                obj['date'] = ApiClient.convertToType(data['date'], 'String');
            }
            if (data.hasOwnProperty('time')) {
                obj['time'] = ApiClient.convertToType(data['time'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} date
 */
GTITime.prototype['date'] = undefined;

/**
 * @member {String} time
 */
GTITime.prototype['time'] = undefined;






export default GTITime;

