/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ScheduleElementLight model module.
 * @module model/ScheduleElementLight
 * @version 38.1.0
 */
class ScheduleElementLight {
    /**
     * Constructs a new <code>ScheduleElementLight</code>.
     * @alias module:model/ScheduleElementLight
     * @param departureStationId {String} 
     * @param arrivalStationId {String} 
     * @param lineId {String} 
     */
    constructor(departureStationId, arrivalStationId, lineId) { 
        
        ScheduleElementLight.initialize(this, departureStationId, arrivalStationId, lineId);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, departureStationId, arrivalStationId, lineId) { 
        obj['departureStationId'] = departureStationId;
        obj['arrivalStationId'] = arrivalStationId;
        obj['lineId'] = lineId;
    }

    /**
     * Constructs a <code>ScheduleElementLight</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ScheduleElementLight} obj Optional instance to populate.
     * @return {module:model/ScheduleElementLight} The populated <code>ScheduleElementLight</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ScheduleElementLight();

            if (data.hasOwnProperty('departureStationId')) {
                obj['departureStationId'] = ApiClient.convertToType(data['departureStationId'], 'String');
            }
            if (data.hasOwnProperty('arrivalStationId')) {
                obj['arrivalStationId'] = ApiClient.convertToType(data['arrivalStationId'], 'String');
            }
            if (data.hasOwnProperty('lineId')) {
                obj['lineId'] = ApiClient.convertToType(data['lineId'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} departureStationId
 */
ScheduleElementLight.prototype['departureStationId'] = undefined;

/**
 * @member {String} arrivalStationId
 */
ScheduleElementLight.prototype['arrivalStationId'] = undefined;

/**
 * @member {String} lineId
 */
ScheduleElementLight.prototype['lineId'] = undefined;






export default ScheduleElementLight;

