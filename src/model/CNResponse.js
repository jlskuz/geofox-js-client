/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import RegionalSDName from './RegionalSDName';

/**
 * The CNResponse model module.
 * @module model/CNResponse
 * @version 38.1.0
 */
class CNResponse {
    /**
     * Constructs a new <code>CNResponse</code>.
     * @alias module:model/CNResponse
     * @param returnCode {module:model/CNResponse.ReturnCodeEnum} 
     */
    constructor(returnCode) { 
        
        CNResponse.initialize(this, returnCode);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, returnCode) { 
        obj['returnCode'] = returnCode;
    }

    /**
     * Constructs a <code>CNResponse</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CNResponse} obj Optional instance to populate.
     * @return {module:model/CNResponse} The populated <code>CNResponse</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CNResponse();

            if (data.hasOwnProperty('returnCode')) {
                obj['returnCode'] = ApiClient.convertToType(data['returnCode'], 'String');
            }
            if (data.hasOwnProperty('errorText')) {
                obj['errorText'] = ApiClient.convertToType(data['errorText'], 'String');
            }
            if (data.hasOwnProperty('errorDevInfo')) {
                obj['errorDevInfo'] = ApiClient.convertToType(data['errorDevInfo'], 'String');
            }
            if (data.hasOwnProperty('results')) {
                obj['results'] = ApiClient.convertToType(data['results'], [RegionalSDName]);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/CNResponse.ReturnCodeEnum} returnCode
 */
CNResponse.prototype['returnCode'] = undefined;

/**
 * @member {String} errorText
 */
CNResponse.prototype['errorText'] = undefined;

/**
 * @member {String} errorDevInfo
 */
CNResponse.prototype['errorDevInfo'] = undefined;

/**
 * @member {Array.<module:model/RegionalSDName>} results
 */
CNResponse.prototype['results'] = undefined;





/**
 * Allowed values for the <code>returnCode</code> property.
 * @enum {String}
 * @readonly
 */
CNResponse['ReturnCodeEnum'] = {

    /**
     * value: "OK"
     * @const
     */
    "OK": "OK",

    /**
     * value: "ERROR_ROUTE"
     * @const
     */
    "ERROR_ROUTE": "ERROR_ROUTE",

    /**
     * value: "ERROR_COMM"
     * @const
     */
    "ERROR_COMM": "ERROR_COMM",

    /**
     * value: "ERROR_CN_TOO_MANY"
     * @const
     */
    "ERROR_CN_TOO_MANY": "ERROR_CN_TOO_MANY",

    /**
     * value: "ERROR_TEXT"
     * @const
     */
    "ERROR_TEXT": "ERROR_TEXT",

    /**
     * value: "START_DEST_TOO_CLOSE"
     * @const
     */
    "START_DEST_TOO_CLOSE": "START_DEST_TOO_CLOSE"
};



export default CNResponse;

