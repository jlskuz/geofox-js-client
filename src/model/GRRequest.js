/**
 * Geofox Api
 * Das GEOFOX Thin Interface, im folgenden GTI genannt, ist eine REST-ähnliche Web-Service-Schnittstelle für GEOFOX.
 *
 * The version of the OpenAPI document: 38.1.0
 * Contact: api@geofox.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import ContSearchByServiceId from './ContSearchByServiceId';
import GTITime from './GTITime';
import Penalty from './Penalty';
import SDName from './SDName';
import TariffInfoSelector from './TariffInfoSelector';

/**
 * The GRRequest model module.
 * @module model/GRRequest
 * @version 38.1.0
 */
class GRRequest {
    /**
     * Constructs a new <code>GRRequest</code>.
     * @alias module:model/GRRequest
     * @param filterType {module:model/GRRequest.FilterTypeEnum}
     * @param start {module:model/SDName}
     * @param dest {module:model/SDName}
     * @param time {module:model/GTITime}
     */
    constructor(filterType, start, dest, time) {

        GRRequest.initialize(this, filterType, start, dest, time);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, filterType, start, dest, time) {
        obj['filterType'] = filterType;
        obj['start'] = start;
        obj['dest'] = dest;
        obj['time'] = time;
    }

    /**
     * Constructs a <code>GRRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GRRequest} obj Optional instance to populate.
     * @return {module:model/GRRequest} The populated <code>GRRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new GRRequest();

            if (data.hasOwnProperty('language')) {
                obj['language'] = ApiClient.convertToType(data['language'], 'String');
            }
            if (data.hasOwnProperty('version')) {
                obj['version'] = ApiClient.convertToType(data['version'], 'Number');
            }
            if (data.hasOwnProperty('filterType')) {
                obj['filterType'] = ApiClient.convertToType(data['filterType'], 'String');
            }
            if (data.hasOwnProperty('start')) {
                obj['start'] = SDName.constructFromObject(data['start']);
            }
            if (data.hasOwnProperty('dest')) {
                obj['dest'] = SDName.constructFromObject(data['dest']);
            }
            if (data.hasOwnProperty('via')) {
                obj['via'] = SDName.constructFromObject(data['via']);
            }
            if (data.hasOwnProperty('time')) {
                obj['time'] = GTITime.constructFromObject(data['time']);
            }
            if (data.hasOwnProperty('timeIsDeparture')) {
                obj['timeIsDeparture'] = ApiClient.convertToType(data['timeIsDeparture'], 'Boolean');
            }
            if (data.hasOwnProperty('numberOfSchedules')) {
                obj['numberOfSchedules'] = ApiClient.convertToType(data['numberOfSchedules'], 'Number');
            }
            if (data.hasOwnProperty('penalties')) {
                obj['penalties'] = ApiClient.convertToType(data['penalties'], [Penalty]);
            }
            if (data.hasOwnProperty('tariffDetails')) {
                obj['tariffDetails'] = ApiClient.convertToType(data['tariffDetails'], 'Boolean');
            }
            if (data.hasOwnProperty('continousSearch')) {
                obj['continousSearch'] = ApiClient.convertToType(data['continousSearch'], 'Boolean');
            }
            if (data.hasOwnProperty('contSearchByServiceId')) {
                obj['contSearchByServiceId'] = ContSearchByServiceId.constructFromObject(data['contSearchByServiceId']);
            }
            if (data.hasOwnProperty('coordinateType')) {
                obj['coordinateType'] = ApiClient.convertToType(data['coordinateType'], 'String');
            }
            if (data.hasOwnProperty('schedulesBefore')) {
                obj['schedulesBefore'] = ApiClient.convertToType(data['schedulesBefore'], 'Number');
            }
            if (data.hasOwnProperty('schedulesAfter')) {
                obj['schedulesAfter'] = ApiClient.convertToType(data['schedulesAfter'], 'Number');
            }
            if (data.hasOwnProperty('tariffInfoSelector')) {
                obj['tariffInfoSelector'] = ApiClient.convertToType(data['tariffInfoSelector'], [TariffInfoSelector]);
            }
            if (data.hasOwnProperty('returnReduced')) {
                obj['returnReduced'] = ApiClient.convertToType(data['returnReduced'], 'Boolean');
            }
            if (data.hasOwnProperty('returnPartialTickets')) {
                obj['returnPartialTickets'] = ApiClient.convertToType(data['returnPartialTickets'], 'Boolean');
            }
            if (data.hasOwnProperty('realtime')) {
                obj['realtime'] = ApiClient.convertToType(data['realtime'], 'String');
            }
            if (data.hasOwnProperty('intermediateStops')) {
                obj['intermediateStops'] = ApiClient.convertToType(data['intermediateStops'], 'Boolean');
            }
            if (data.hasOwnProperty('useStationPosition')) {
                obj['useStationPosition'] = ApiClient.convertToType(data['useStationPosition'], 'Boolean');
            }
            if (data.hasOwnProperty('forcedStart')) {
                obj['forcedStart'] = SDName.constructFromObject(data['forcedStart']);
            }
            if (data.hasOwnProperty('forcedDest')) {
                obj['forcedDest'] = SDName.constructFromObject(data['forcedDest']);
            }
            if (data.hasOwnProperty('toStartBy')) {
                obj['toStartBy'] = ApiClient.convertToType(data['toStartBy'], 'String');
            }
            if (data.hasOwnProperty('toDestBy')) {
                obj['toDestBy'] = ApiClient.convertToType(data['toDestBy'], 'String');
            }
            if (data.hasOwnProperty('returnContSearchData')) {
                obj['returnContSearchData'] = ApiClient.convertToType(data['returnContSearchData'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {String} language
 * @default 'de'
 */
GRRequest.prototype['language'] = 'de';

/**
 * @member {Number} version
 * @default 38
 */
GRRequest.prototype['version'] = 38;

/**
 * @member {module:model/GRRequest.FilterTypeEnum} filterType
 * @default 'NO_FILTER'
 */
GRRequest.prototype['filterType'] = 'NO_FILTER';

/**
 * @member {module:model/SDName} start
 */
GRRequest.prototype['start'] = undefined;

/**
 * @member {module:model/SDName} dest
 */
GRRequest.prototype['dest'] = undefined;

/**
 * @member {module:model/SDName} via
 */
GRRequest.prototype['via'] = undefined;

/**
 * @member {module:model/GTITime} time
 */
GRRequest.prototype['time'] = undefined;

/**
 * @member {Boolean} timeIsDeparture
 */
GRRequest.prototype['timeIsDeparture'] = undefined;

/**
 * @member {Number} numberOfSchedules
 */
GRRequest.prototype['numberOfSchedules'] = undefined;

/**
 * @member {Array.<module:model/Penalty>} penalties
 */
GRRequest.prototype['penalties'] = undefined;

/**
 * @member {Boolean} tariffDetails
 * @default false
 */
GRRequest.prototype['tariffDetails'] = false;

/**
 * @member {Boolean} continousSearch
 * @default false
 */
GRRequest.prototype['continousSearch'] = false;

/**
 * @member {module:model/ContSearchByServiceId} contSearchByServiceId
 */
GRRequest.prototype['contSearchByServiceId'] = undefined;

/**
 * @member {module:model/GRRequest.CoordinateTypeEnum} coordinateType
 * @default 'EPSG_4326'
 */
GRRequest.prototype['coordinateType'] = 'EPSG_4326';

/**
 * @member {Number} schedulesBefore
 * @default 0
 */
GRRequest.prototype['schedulesBefore'] = 0;

/**
 * @member {Number} schedulesAfter
 * @default 0
 */
GRRequest.prototype['schedulesAfter'] = 0;

/**
 * @member {Array.<module:model/TariffInfoSelector>} tariffInfoSelector
 */
GRRequest.prototype['tariffInfoSelector'] = undefined;

/**
 * @member {Boolean} returnReduced
 * @default false
 */
GRRequest.prototype['returnReduced'] = false;

/**
 * @member {Boolean} returnPartialTickets
 * @default true
 */
GRRequest.prototype['returnPartialTickets'] = true;

/**
 * @member {module:model/GRRequest.RealtimeEnum} realtime
 * @default 'AUTO'
 */
GRRequest.prototype['realtime'] = 'AUTO';

/**
 * @member {Boolean} intermediateStops
 * @default false
 */
GRRequest.prototype['intermediateStops'] = false;

/**
 * @member {Boolean} useStationPosition
 * @default true
 */
GRRequest.prototype['useStationPosition'] = true;

/**
 * @member {module:model/SDName} forcedStart
 */
GRRequest.prototype['forcedStart'] = undefined;

/**
 * @member {module:model/SDName} forcedDest
 */
GRRequest.prototype['forcedDest'] = undefined;

/**
 * @member {module:model/GRRequest.ToStartByEnum} toStartBy
 */
GRRequest.prototype['toStartBy'] = undefined;

/**
 * @member {module:model/GRRequest.ToDestByEnum} toDestBy
 */
GRRequest.prototype['toDestBy'] = undefined;

/**
 * @member {Boolean} returnContSearchData
 */
GRRequest.prototype['returnContSearchData'] = undefined;





/**
 * Allowed values for the <code>filterType</code> property.
 * @enum {String}
 * @readonly
 */
GRRequest['FilterTypeEnum'] = {

    /**
     * value: "NO_FILTER"
     * @const
     */
    "NO_FILTER": "NO_FILTER",

    /**
     * value: "HVV_LISTED"
     * @const
     */
    "HVV_LISTED": "HVV_LISTED"
};


/**
 * Allowed values for the <code>coordinateType</code> property.
 * @enum {String}
 * @readonly
 */
GRRequest['CoordinateTypeEnum'] = {

    /**
     * value: "EPSG_4326"
     * @const
     */
    "4326": "EPSG_4326",

    /**
     * value: "EPSG_31466"
     * @const
     */
    "31466": "EPSG_31466",

    /**
     * value: "EPSG_31467"
     * @const
     */
    "31467": "EPSG_31467",

    /**
     * value: "EPSG_31468"
     * @const
     */
    "31468": "EPSG_31468",

    /**
     * value: "EPSG_31469"
     * @const
     */
    "31469": "EPSG_31469"
};


/**
 * Allowed values for the <code>realtime</code> property.
 * @enum {String}
 * @readonly
 */
GRRequest['RealtimeEnum'] = {

    /**
     * value: "PLANDATA"
     * @const
     */
    "PLANDATA": "PLANDATA",

    /**
     * value: "REALTIME"
     * @const
     */
    "REALTIME": "REALTIME",

    /**
     * value: "AUTO"
     * @const
     */
    "AUTO": "AUTO"
};


/**
 * Allowed values for the <code>toStartBy</code> property.
 * @enum {String}
 * @readonly
 */
GRRequest['ToStartByEnum'] = {

    /**
     * value: "BUS"
     * @const
     */
    "BUS": "BUS",

    /**
     * value: "TRAIN"
     * @const
     */
    "TRAIN": "TRAIN",

    /**
     * value: "SHIP"
     * @const
     */
    "SHIP": "SHIP",

    /**
     * value: "FOOTPATH"
     * @const
     */
    "FOOTPATH": "FOOTPATH",

    /**
     * value: "BICYCLE"
     * @const
     */
    "BICYCLE": "BICYCLE",

    /**
     * value: "AIRPLANE"
     * @const
     */
    "AIRPLANE": "AIRPLANE",

    /**
     * value: "CHANGE"
     * @const
     */
    "CHANGE": "CHANGE",

    /**
     * value: "CHANGE_SAME_PLATFORM"
     * @const
     */
    "CHANGE_SAME_PLATFORM": "CHANGE_SAME_PLATFORM"
};


/**
 * Allowed values for the <code>toDestBy</code> property.
 * @enum {String}
 * @readonly
 */
GRRequest['ToDestByEnum'] = {

    /**
     * value: "BUS"
     * @const
     */
    "BUS": "BUS",

    /**
     * value: "TRAIN"
     * @const
     */
    "TRAIN": "TRAIN",

    /**
     * value: "SHIP"
     * @const
     */
    "SHIP": "SHIP",

    /**
     * value: "FOOTPATH"
     * @const
     */
    "FOOTPATH": "FOOTPATH",

    /**
     * value: "BICYCLE"
     * @const
     */
    "BICYCLE": "BICYCLE",

    /**
     * value: "AIRPLANE"
     * @const
     */
    "AIRPLANE": "AIRPLANE",

    /**
     * value: "CHANGE"
     * @const
     */
    "CHANGE": "CHANGE",

    /**
     * value: "CHANGE_SAME_PLATFORM"
     * @const
     */
    "CHANGE_SAME_PLATFORM": "CHANGE_SAME_PLATFORM"
};



export default GRRequest;
