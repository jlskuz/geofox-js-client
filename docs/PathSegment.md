# GeofoxApi.PathSegment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startStopPointKey** | **String** |  | 
**endStopPointKey** | **String** |  | 
**startStationName** | **String** |  | 
**startStationKey** | **String** |  | 
**startDateTime** | **Number** |  | [optional] 
**endStationName** | **String** |  | 
**endStationKey** | **String** |  | 
**endDateTime** | **Number** |  | [optional] 
**track** | [**VehicleMapPath**](VehicleMapPath.md) |  | 
**destination** | **String** |  | 
**realtimeDelay** | **Number** |  | [optional] 
**isFirst** | **Boolean** |  | [optional] 
**isLast** | **Boolean** |  | [optional] 


