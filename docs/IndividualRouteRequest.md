# GeofoxApi.IndividualRouteRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  | [optional] [default to &#39;de&#39;]
**version** | **Number** |  | [optional] [default to 38]
**filterType** | **String** |  | [default to &#39;NO_FILTER&#39;]
**starts** | [**[SDName]**](SDName.md) |  | [optional] 
**dests** | [**[SDName]**](SDName.md) |  | [optional] 
**maxLength** | **Number** |  | [optional] 
**maxResults** | **Number** |  | [optional] 
**type** | **String** |  | [optional] [default to &#39;EPSG_4326&#39;]
**serviceType** | **String** |  | [optional] [default to &#39;FOOTPATH&#39;]
**profile** | **String** |  | [optional] [default to &#39;FOOT_NORMAL&#39;]
**speed** | **String** |  | [optional] [default to &#39;NORMAL&#39;]



## Enum: FilterTypeEnum


* `NO_FILTER` (value: `"NO_FILTER"`)

* `HVV_LISTED` (value: `"HVV_LISTED"`)





## Enum: TypeEnum


* `4326` (value: `"EPSG_4326"`)

* `31466` (value: `"EPSG_31466"`)

* `31467` (value: `"EPSG_31467"`)

* `31468` (value: `"EPSG_31468"`)

* `31469` (value: `"EPSG_31469"`)





## Enum: ServiceTypeEnum


* `BUS` (value: `"BUS"`)

* `TRAIN` (value: `"TRAIN"`)

* `SHIP` (value: `"SHIP"`)

* `FOOTPATH` (value: `"FOOTPATH"`)

* `BICYCLE` (value: `"BICYCLE"`)

* `AIRPLANE` (value: `"AIRPLANE"`)

* `CHANGE` (value: `"CHANGE"`)

* `CHANGE_SAME_PLATFORM` (value: `"CHANGE_SAME_PLATFORM"`)





## Enum: ProfileEnum


* `BICYCLE_NORMAL` (value: `"BICYCLE_NORMAL"`)

* `BICYCLE_RACING` (value: `"BICYCLE_RACING"`)

* `BICYCLE_QUIET_ROADS` (value: `"BICYCLE_QUIET_ROADS"`)

* `BICYCLE_MAIN_ROADS` (value: `"BICYCLE_MAIN_ROADS"`)

* `BICYCLE_BAD_WEATHER` (value: `"BICYCLE_BAD_WEATHER"`)

* `FOOT_NORMAL` (value: `"FOOT_NORMAL"`)




