# GeofoxApi.SingleTicketOptimizerRequestRoute

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trip** | [**[SingleTicketOptimizerRequestTrip]**](SingleTicketOptimizerRequestTrip.md) |  | 
**departure** | **Date** |  | 
**arrival** | **Date** |  | 
**tariffRegions** | [**TariffOptimizerRegions**](TariffOptimizerRegions.md) |  | 
**singleTicketTariffLevelId** | **Number** |  | [optional] 
**extraFareType** | **String** |  | 



## Enum: ExtraFareTypeEnum


* `NO` (value: `"NO"`)

* `POSSIBLE` (value: `"POSSIBLE"`)

* `REQUIRED` (value: `"REQUIRED"`)




