# GeofoxApi.TariffOptimizerRegions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**zones** | [**[TariffRegions]**](TariffRegions.md) |  | [optional] 
**rings** | [**[TariffRegions]**](TariffRegions.md) |  | [optional] 
**counties** | [**[TariffRegions]**](TariffRegions.md) |  | [optional] 


