# GeofoxApi.TariffZone

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**zone** | **String** |  | 
**ring** | **String** |  | 
**neighbours** | **[String]** |  | 


