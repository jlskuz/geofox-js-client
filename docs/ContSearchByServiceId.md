# GeofoxApi.ContSearchByServiceId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serviceId** | **Number** |  | [optional] 
**lineKey** | **String** |  | 
**plannedDepArrTime** | [**GTITime**](GTITime.md) |  | 
**additionalOffset** | **Number** |  | [optional] 


