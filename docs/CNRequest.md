# GeofoxApi.CNRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  | [optional] [default to &#39;de&#39;]
**version** | **Number** |  | [optional] [default to 38]
**filterType** | **String** |  | [default to &#39;NO_FILTER&#39;]
**theName** | [**SDName**](SDName.md) |  | 
**maxList** | **Number** |  | [optional] 
**maxDistance** | **Number** |  | [optional] 
**coordinateType** | **String** |  | [optional] [default to &#39;EPSG_4326&#39;]
**tariffDetails** | **Boolean** |  | [optional] [default to false]
**allowTypeSwitch** | **Boolean** |  | [optional] [default to true]



## Enum: FilterTypeEnum


* `NO_FILTER` (value: `"NO_FILTER"`)

* `HVV_LISTED` (value: `"HVV_LISTED"`)





## Enum: CoordinateTypeEnum


* `4326` (value: `"EPSG_4326"`)

* `31466` (value: `"EPSG_31466"`)

* `31467` (value: `"EPSG_31467"`)

* `31468` (value: `"EPSG_31468"`)

* `31469` (value: `"EPSG_31469"`)




