# GeofoxApi.Departure

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line** | [**Service**](Service.md) |  | 
**timeOffset** | **Number** |  | [optional] 
**delay** | **Number** |  | [optional] 
**extra** | **Boolean** |  | [optional] [default to false]
**cancelled** | **Boolean** |  | [optional] [default to false]
**serviceId** | **Number** |  | [optional] 
**station** | [**SDName**](SDName.md) |  | [optional] 
**platform** | **String** |  | [optional] 
**realtimePlatform** | **String** |  | [optional] 
**attributes** | [**[Attribute]**](Attribute.md) |  | [optional] 


