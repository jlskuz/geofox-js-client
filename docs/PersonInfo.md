# GeofoxApi.PersonInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**personType** | **String** |  | [optional] 
**personCount** | **Number** |  | [optional] 



## Enum: PersonTypeEnum


* `ALL` (value: `"ALL"`)

* `ELDERLY` (value: `"ELDERLY"`)

* `APPRENTICE` (value: `"APPRENTICE"`)

* `PUPIL` (value: `"PUPIL"`)

* `STUDENT` (value: `"STUDENT"`)

* `CHILD` (value: `"CHILD"`)




