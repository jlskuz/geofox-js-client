# GeofoxApi.Coordinate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**x** | **Number** |  | [optional] 
**y** | **Number** |  | [optional] 
**type** | **String** |  | [optional] [default to &#39;EPSG_4326&#39;]



## Enum: TypeEnum


* `4326` (value: `"EPSG_4326"`)

* `31466` (value: `"EPSG_31466"`)

* `31467` (value: `"EPSG_31467"`)

* `31468` (value: `"EPSG_31468"`)

* `31469` (value: `"EPSG_31469"`)




