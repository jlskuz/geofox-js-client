# GeofoxApi.ShopInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shopType** | **String** |  | 
**url** | **String** |  | 



## Enum: ShopTypeEnum


* `AST` (value: `"AST"`)




