# GeofoxApi.SingleTicketOptimizerRequestTrip

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | [**SingleTicketOptimizerRequestStation**](SingleTicketOptimizerRequestStation.md) |  | 
**destination** | [**SingleTicketOptimizerRequestStation**](SingleTicketOptimizerRequestStation.md) |  | 
**line** | [**SingleTicketOptimizerRequestLine**](SingleTicketOptimizerRequestLine.md) |  | 
**vehicleType** | **String** |  | 


