# GeofoxApi.TrackCoordsControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTrackCoordinatesPublic**](TrackCoordsControllerApi.md#getTrackCoordinatesPublic) | **POST** /gti/public/getTrackCoordinates | 



## getTrackCoordinatesPublic

> TrackCoordinatesResponse getTrackCoordinatesPublic(trackCoordinatesRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.TrackCoordsControllerApi();
let trackCoordinatesRequest = new GeofoxApi.TrackCoordinatesRequest(); // TrackCoordinatesRequest | 
apiInstance.getTrackCoordinatesPublic(trackCoordinatesRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **trackCoordinatesRequest** | [**TrackCoordinatesRequest**](TrackCoordinatesRequest.md)|  | 

### Return type

[**TrackCoordinatesResponse**](TrackCoordinatesResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

