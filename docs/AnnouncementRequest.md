# GeofoxApi.AnnouncementRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  | [optional] [default to &#39;de&#39;]
**version** | **Number** |  | [optional] [default to 38]
**filterType** | **String** |  | [default to &#39;NO_FILTER&#39;]
**names** | **[String]** |  | [optional] 
**timeRange** | [**TimeRange**](TimeRange.md) |  | [optional] 
**full** | **Boolean** |  | [optional] [default to false]
**filterPlanned** | **String** |  | [optional] 



## Enum: FilterTypeEnum


* `NO_FILTER` (value: `"NO_FILTER"`)

* `HVV_LISTED` (value: `"HVV_LISTED"`)





## Enum: FilterPlannedEnum


* `NO_FILTER` (value: `"NO_FILTER"`)

* `ONLY_PLANNED` (value: `"ONLY_PLANNED"`)

* `ONLY_UNPLANNED` (value: `"ONLY_UNPLANNED"`)




