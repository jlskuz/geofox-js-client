# GeofoxApi.IndividualRoute

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | [**SDName**](SDName.md) |  | 
**dest** | [**SDName**](SDName.md) |  | 
**path** | [**Path**](Path.md) |  | [optional] 
**paths** | [**[Path]**](Path.md) |  | [optional] 
**length** | **Number** |  | [optional] 
**time** | **Number** |  | [optional] 
**serviceType** | **String** |  | [default to &#39;FOOTPATH&#39;]



## Enum: ServiceTypeEnum


* `BUS` (value: `"BUS"`)

* `TRAIN` (value: `"TRAIN"`)

* `SHIP` (value: `"SHIP"`)

* `FOOTPATH` (value: `"FOOTPATH"`)

* `BICYCLE` (value: `"BICYCLE"`)

* `AIRPLANE` (value: `"AIRPLANE"`)

* `CHANGE` (value: `"CHANGE"`)

* `CHANGE_SAME_PLATFORM` (value: `"CHANGE_SAME_PLATFORM"`)




