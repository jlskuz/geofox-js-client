# GeofoxApi.CheckNameControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkName**](CheckNameControllerApi.md#checkName) | **POST** /gti/public/checkName | 



## checkName

> CNResponse checkName(cNRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.CheckNameControllerApi();
let cNRequest = new GeofoxApi.CNRequest(); // CNRequest | 
apiInstance.checkName(cNRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cNRequest** | [**CNRequest**](CNRequest.md)|  | 

### Return type

[**CNResponse**](CNResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

