# GeofoxApi.Announcement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**version** | **Number** |  | [optional] 
**summary** | **String** |  | [optional] 
**locations** | [**[Location]**](Location.md) |  | 
**description** | **String** |  | 
**links** | [**[Link]**](Link.md) |  | [optional] 
**publication** | [**TimeRange**](TimeRange.md) |  | 
**validities** | [**[TimeRange]**](TimeRange.md) |  | 
**lastModified** | **Date** |  | 
**planned** | **Boolean** |  | [optional] 
**reason** | **String** |  | [optional] 


