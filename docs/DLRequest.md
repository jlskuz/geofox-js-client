# GeofoxApi.DLRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  | [optional] [default to &#39;de&#39;]
**version** | **Number** |  | [optional] [default to 38]
**filterType** | **String** |  | [default to &#39;NO_FILTER&#39;]
**station** | [**SDName**](SDName.md) |  | [optional] 
**stations** | [**[SDName]**](SDName.md) |  | [optional] 
**time** | [**GTITime**](GTITime.md) |  | 
**maxList** | **Number** |  | [optional] 
**maxTimeOffset** | **Number** |  | [optional] [default to 120]
**allStationsInChangingNode** | **Boolean** |  | [optional] [default to true]
**useRealtime** | **Boolean** |  | [optional] [default to false]
**returnFilters** | **Boolean** |  | [optional] [default to false]
**filter** | [**[DLFilterEntry]**](DLFilterEntry.md) |  | [optional] 
**serviceTypes** | **[String]** |  | [optional] 
**departure** | **Boolean** |  | [optional] [default to true]



## Enum: FilterTypeEnum


* `NO_FILTER` (value: `"NO_FILTER"`)

* `HVV_LISTED` (value: `"HVV_LISTED"`)





## Enum: [ServiceTypesEnum]


* `ZUG` (value: `"ZUG"`)

* `UBAHN` (value: `"UBAHN"`)

* `SBAHN` (value: `"SBAHN"`)

* `AKN` (value: `"AKN"`)

* `RBAHN` (value: `"RBAHN"`)

* `FERNBAHN` (value: `"FERNBAHN"`)

* `BUS` (value: `"BUS"`)

* `STADTBUS` (value: `"STADTBUS"`)

* `METROBUS` (value: `"METROBUS"`)

* `SCHNELLBUS` (value: `"SCHNELLBUS"`)

* `NACHTBUS` (value: `"NACHTBUS"`)

* `XPRESSBUS` (value: `"XPRESSBUS"`)

* `EILBUS` (value: `"EILBUS"`)

* `AST` (value: `"AST"`)

* `FAEHRE` (value: `"FAEHRE"`)




