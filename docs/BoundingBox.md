# GeofoxApi.BoundingBox

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lowerLeft** | [**Coordinate**](Coordinate.md) |  | 
**upperRight** | [**Coordinate**](Coordinate.md) |  | 


