# GeofoxApi.Ticket

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | **Number** |  | [optional] 
**reducedPrice** | **Number** |  | [optional] 
**currency** | **String** |  | [optional] [default to &#39;EUR&#39;]
**type** | **String** |  | 
**level** | **String** |  | 
**tariff** | **String** |  | 
**range** | **String** |  | 
**ticketRemarks** | **String** |  | [optional] 


