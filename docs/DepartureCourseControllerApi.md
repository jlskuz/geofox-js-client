# GeofoxApi.DepartureCourseControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**departureCourse**](DepartureCourseControllerApi.md#departureCourse) | **POST** /gti/public/departureCourse | 



## departureCourse

> DCResponse departureCourse(dCRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.DepartureCourseControllerApi();
let dCRequest = new GeofoxApi.DCRequest(); // DCRequest | 
apiInstance.departureCourse(dCRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dCRequest** | [**DCRequest**](DCRequest.md)|  | 

### Return type

[**DCResponse**](DCResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

