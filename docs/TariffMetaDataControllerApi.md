# GeofoxApi.TariffMetaDataControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tariffMetaData1**](TariffMetaDataControllerApi.md#tariffMetaData1) | **POST** /gti/public/tariffMetaData | 



## tariffMetaData1

> TariffMetaDataResponse tariffMetaData1(tariffMetaDataRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.TariffMetaDataControllerApi();
let tariffMetaDataRequest = new GeofoxApi.TariffMetaDataRequest(); // TariffMetaDataRequest | 
apiInstance.tariffMetaData1(tariffMetaDataRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tariffMetaDataRequest** | [**TariffMetaDataRequest**](TariffMetaDataRequest.md)|  | 

### Return type

[**TariffMetaDataResponse**](TariffMetaDataResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

