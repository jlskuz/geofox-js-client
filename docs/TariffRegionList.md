# GeofoxApi.TariffRegionList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**regions** | **[String]** |  | [optional] 


