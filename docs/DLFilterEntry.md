# GeofoxApi.DLFilterEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serviceID** | **String** |  | [optional] 
**stationIDs** | **[String]** |  | [optional] 
**label** | **String** |  | [optional] 
**serviceName** | **String** |  | [optional] 


