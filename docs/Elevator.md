# GeofoxApi.Elevator

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lines** | **[String]** |  | [optional] 
**label** | **String** |  | [optional] 
**cabinWidth** | **Number** |  | [optional] 
**cabinLength** | **Number** |  | [optional] 
**doorWidth** | **Number** |  | [optional] 
**description** | **String** |  | [optional] 
**elevatorType** | **String** |  | [optional] 
**buttonType** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**cause** | **String** |  | [optional] 



## Enum: ButtonTypeEnum


* `BRAILLE` (value: `"BRAILLE"`)

* `ACUSTIC` (value: `"ACUSTIC"`)

* `COMBI` (value: `"COMBI"`)

* `UNKNOWN` (value: `"UNKNOWN"`)





## Enum: StateEnum


* `READY` (value: `"READY"`)

* `OUTOFORDER` (value: `"OUTOFORDER"`)

* `UNKNOWN` (value: `"UNKNOWN"`)




