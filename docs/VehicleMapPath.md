# GeofoxApi.VehicleMapPath

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**track** | **[Number]** |  | [optional] 
**coordinateType** | **String** |  | 



## Enum: CoordinateTypeEnum


* `4326` (value: `"EPSG_4326"`)

* `31466` (value: `"EPSG_31466"`)

* `31467` (value: `"EPSG_31467"`)

* `31468` (value: `"EPSG_31468"`)

* `31469` (value: `"EPSG_31469"`)




