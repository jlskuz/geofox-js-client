# GeofoxApi.TariffLevel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**label** | **String** |  | 
**requiredRegionType** | [**RequiredRegionType**](RequiredRegionType.md) |  | 


