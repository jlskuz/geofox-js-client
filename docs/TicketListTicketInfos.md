# GeofoxApi.TicketListTicketInfos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tariffKindID** | **Number** |  | [optional] 
**tariffKindLabel** | **String** |  | 
**tariffLevelID** | **Number** |  | [optional] 
**tariffLevelLabel** | **String** |  | 
**tariffGroupID** | **Number** |  | [optional] 
**tariffGroupLabel** | **String** |  | [optional] 
**regionType** | **String** |  | [optional] 
**selectableRegions** | **Number** |  | [optional] [default to 0]
**requiredStartStation** | **Boolean** |  | [optional] [default to false]
**personInfos** | [**[PersonInfo]**](PersonInfo.md) |  | [optional] 
**validityPeriods** | [**[ValidityPeriod]**](ValidityPeriod.md) |  | [optional] 
**variants** | [**[TicketListTicketVariant]**](TicketListTicketVariant.md) |  | [optional] 



## Enum: RegionTypeEnum


* `ZONE` (value: `"ZONE"`)

* `GH_ZONE` (value: `"GH_ZONE"`)

* `RING` (value: `"RING"`)

* `COUNTY` (value: `"COUNTY"`)

* `GH` (value: `"GH"`)

* `NET` (value: `"NET"`)

* `ZG` (value: `"ZG"`)

* `STADTVERKEHR` (value: `"STADTVERKEHR"`)




