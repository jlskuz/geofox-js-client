# GeofoxApi.ScheduleElement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | [**JourneySDName**](JourneySDName.md) |  | 
**to** | [**JourneySDName**](JourneySDName.md) |  | 
**line** | [**Service**](Service.md) |  | 
**paths** | [**[Path]**](Path.md) |  | [optional] 
**attributes** | [**[Attribute]**](Attribute.md) |  | [optional] 
**extra** | **Boolean** |  | [optional] [default to false]
**cancelled** | **Boolean** |  | [optional] [default to false]
**intermediateStops** | [**[JourneySDName]**](JourneySDName.md) |  | [optional] 
**serviceId** | **Number** |  | [optional] 
**shopInfo** | [**[ShopInfo]**](ShopInfo.md) |  | [optional] 


