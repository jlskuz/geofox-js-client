# GeofoxApi.SDName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**combinedName** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**type** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**coordinate** | [**Coordinate**](Coordinate.md) |  | [optional] 
**tariffDetails** | [**TariffDetails**](TariffDetails.md) |  | [optional] 
**serviceTypes** | **[String]** |  | [optional] 
**hasStationInformation** | **Boolean** |  | [optional] 



## Enum: TypeEnum


* `UNKNOWN` (value: `"UNKNOWN"`)

* `STATION` (value: `"STATION"`)

* `ADDRESS` (value: `"ADDRESS"`)

* `POI` (value: `"POI"`)

* `COORDINATE` (value: `"COORDINATE"`)




