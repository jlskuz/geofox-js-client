# GeofoxApi.PostalCodeControllerApi

All URIs are relative to *https://gti.geofox.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkPostalCode**](PostalCodeControllerApi.md#checkPostalCode) | **POST** /gti/public/checkPostalCode | 



## checkPostalCode

> PCResponse checkPostalCode(pCRequest)



### Example

```javascript
import GeofoxApi from 'geofox_api';
let defaultClient = GeofoxApi.ApiClient.instance;
// Configure HTTP basic authorization: APIKey1
let APIKey1 = defaultClient.authentications['APIKey1'];
APIKey1.username = 'YOUR USERNAME';
APIKey1.password = 'YOUR PASSWORD';

let apiInstance = new GeofoxApi.PostalCodeControllerApi();
let pCRequest = new GeofoxApi.PCRequest(); // PCRequest | 
apiInstance.checkPostalCode(pCRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pCRequest** | [**PCRequest**](PCRequest.md)|  | 

### Return type

[**PCResponse**](PCResponse.md)

### Authorization

[APIKey1](../README.md#APIKey1)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

