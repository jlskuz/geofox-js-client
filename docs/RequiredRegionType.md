# GeofoxApi.RequiredRegionType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**count** | **Number** |  | [optional] 



## Enum: TypeEnum


* `ZONE` (value: `"ZONE"`)

* `GH_ZONE` (value: `"GH_ZONE"`)

* `RING` (value: `"RING"`)

* `COUNTY` (value: `"COUNTY"`)

* `GH` (value: `"GH"`)

* `NET` (value: `"NET"`)

* `ZG` (value: `"ZG"`)

* `STADTVERKEHR` (value: `"STADTVERKEHR"`)




