# GeofoxApi.TicketInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tariffKindID** | **Number** |  | [optional] 
**tariffKindLabel** | **String** |  | 
**tariffLevelID** | **Number** |  | [optional] 
**tariffLevelLabel** | **String** |  | 
**tariffGroupID** | **Number** |  | [optional] 
**tariffGroupLabel** | **String** |  | [optional] 
**basePrice** | **Number** |  | [optional] 
**extraFarePrice** | **Number** |  | [optional] 
**reducedBasePrice** | **Number** |  | [optional] 
**reducedExtraFarePrice** | **Number** |  | [optional] 
**currency** | **String** |  | [optional] [default to &#39;EUR&#39;]
**regionType** | **String** |  | [optional] 
**notRecommended** | **Boolean** |  | [optional] [default to false]
**shopLinkRegular** | **String** |  | [optional] 
**shopLinkExtraFare** | **String** |  | [optional] 



## Enum: RegionTypeEnum


* `ZONE` (value: `"ZONE"`)

* `GH_ZONE` (value: `"GH_ZONE"`)

* `RING` (value: `"RING"`)

* `COUNTY` (value: `"COUNTY"`)

* `GH` (value: `"GH"`)

* `NET` (value: `"NET"`)

* `ZG` (value: `"ZG"`)

* `STADTVERKEHR` (value: `"STADTVERKEHR"`)




