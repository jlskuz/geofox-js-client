# GeofoxApi.LineListEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**name** | **String** |  | [optional] 
**carrierNameShort** | **String** |  | [optional] 
**carrierNameLong** | **String** |  | [optional] 
**sublines** | [**[SublineListEntry]**](SublineListEntry.md) |  | [optional] 
**exists** | **Boolean** |  | [optional] [default to true]
**type** | [**ServiceType**](ServiceType.md) |  | 


