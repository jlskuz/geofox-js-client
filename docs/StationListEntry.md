# GeofoxApi.StationListEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**name** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**combinedName** | **String** |  | [optional] 
**shortcuts** | **[String]** |  | [optional] 
**aliasses** | **[String]** |  | [optional] 
**vehicleTypes** | **[String]** |  | [optional] 
**coordinate** | [**Coordinate**](Coordinate.md) |  | [optional] 
**exists** | **Boolean** |  | [optional] [default to true]



## Enum: [VehicleTypesEnum]


* `REGIONALBUS` (value: `"REGIONALBUS"`)

* `METROBUS` (value: `"METROBUS"`)

* `NACHTBUS` (value: `"NACHTBUS"`)

* `SCHNELLBUS` (value: `"SCHNELLBUS"`)

* `XPRESSBUS` (value: `"XPRESSBUS"`)

* `AST` (value: `"AST"`)

* `SCHIFF` (value: `"SCHIFF"`)

* `U_BAHN` (value: `"U_BAHN"`)

* `S_BAHN` (value: `"S_BAHN"`)

* `A_BAHN` (value: `"A_BAHN"`)

* `R_BAHN` (value: `"R_BAHN"`)

* `F_BAHN` (value: `"F_BAHN"`)

* `EILBUS` (value: `"EILBUS"`)




