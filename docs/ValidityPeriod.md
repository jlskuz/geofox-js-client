# GeofoxApi.ValidityPeriod

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **String** |  | 
**timeValidities** | [**[TimePeriod]**](TimePeriod.md) |  | [optional] 



## Enum: DayEnum


* `WEEKDAY` (value: `"WEEKDAY"`)

* `WEEKEND` (value: `"WEEKEND"`)




